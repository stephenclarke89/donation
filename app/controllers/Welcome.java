package controllers;

import models.User;
import play.mvc.Controller;

import com.jamonapi.utils.Logger;

public class Welcome extends Controller 
{

	//If user is null render welcome page
	//else render user
	public static void index()
	{
		User user = Accounts.getCurrentUser();
		if (user == null) 
        {
		    Logger.logInfo("Landed in Welcome class");	
		    render();
		} 
		else 
		{
			Logger.logInfo("Landed in Welcome class");
			render(user);
		}
	}
}