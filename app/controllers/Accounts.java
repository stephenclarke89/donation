package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
	//Get current user. If null render index else render user
    public static void index()
    {
    	User user = Accounts.getCurrentUser();
		if (user == null) 
        {
		    render();
		} 
		else 
		{
			render(user);
		}
    }
 
    //Get current user. If null list candidates find all render candidates else render user.
    public static void signup()
    {
	    User user = Accounts.getCurrentUser();
		if (user == null) 
        {
		    List<Candidate> candidates = Candidate.findAll();
			render(candidates);
		} 
		else 
		{
			render(user);
		}
    }

    //Register a users details. Users info sent to command line. 
    //User selects candidate. User is saved then goes to login.
    public static void register
         (boolean usaCitizen, String firstName,
		 String lastName, String state, int age, String email, String password,
		 String addressLine1, String addressLine2, String city, String zipCode, String candidate)
    {
	    Logger.info(usaCitizen + " " + firstName + " " + lastName + " " + state + " " + age + " " + email + " " 
        + password + " " + addressLine1 + " " + addressLine2 + " " + city + " " + zipCode);
	    Candidate selCandidate = Candidate.findById(Long.parseLong(candidate));
	    User user = new User(usaCitizen, firstName, lastName, state, age, email, password, addressLine1, addressLine2, city, zipCode);
	    user.candidate = selCandidate;
	    user.save();
	    Accounts.login();
    }
 
    //Get user. If user is null render else render user.
    public static void login()
    {
	    User user = Accounts.getCurrentUser();
	    if (user == null) 
        {
		    render();
	    } 
	    else 
	    {
		    render(user);
	    }
    }
 
    //To logout clear the session then go to in the index.
    public static void logout()
    {
	    session.clear();
	    index();
    }
 
    //If user is not null and the password is true then go to the donation page
    //else go to login page
    public static void authenticate(String email, String password)
    {
	    Logger.info("Attempting to authenticate with " + email + ":" + password);
	    User user = User.findByEmail(email);
	    if ((user != null) && (user.checkPassword(password) == true))
        {
	        Logger.info("Successfull authentication of " + user.firstName + " " + user.lastName);
	        session.put("logged_in_userid", user.id);
	        DonationController.index();
        }
        else
        {
	        Logger.info("Authentication failed");
	        login();
        }
    }
 
    //If userId is null then return null
    //return logged_in_user
    public static User getCurrentUser()
    {
	    String userId = session.get("logged_in_userid");
	    if (userId == null)
        {
	        return null;
        }
 	    User logged_in_user = User.findById(Long.parseLong(userId));
 	    Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
 	    return logged_in_user;
    }
}