package controllers;

import models.Candidate;
import models.Donation;
import models.User;
import play.mvc.Controller;

import java.util.ArrayList;
import java.util.List;

import com.jamonapi.utils.Logger;

public class DonationController extends Controller 
{
	//If user is null then go to accounts.login page
	//else list candidates. Render user, progress candidate
	public static void index() 
	{
		User user = Accounts.getCurrentUser();
		if (user == null) 
        {
		     Logger.logInfo("Donation class : Unable to getCurrentuser");
		     Accounts.login();
		} 
		else 
		{
		    List<Candidate> candidates = Candidate.findAll();
			String prog = getPercentTargetAchieved();
			String progress = prog + "%";
			Logger.logInfo("Donation ctrler : user is " + user.email);
			Logger.logInfo("Donation ctrler : percent target achieved " + progress);
			render(user, progress, candidates);
		}
    }

	//If user is null then go to accounts.login page
	//else add donation user, amount donated method go to index
	public static void donate(long amountDonated, String methodDonated) 
	{
		Logger.logInfo("amount donated " + amountDonated + " " + "method donated " + methodDonated);
		User user = Accounts.getCurrentUser();
		if (user == null) 
		{
			Logger.logInfo("Donation class : Unable to getCurrentuser");
			Accounts.login();
		} 
		else 
		{
			addDonation(user, amountDonated, methodDonated);
		}
		index();
	}

	//Add a new donation then save the balance
	private static void addDonation(User user, long amountDonated, String methodDonated) 
	{
		Donation bal = new Donation(user, amountDonated, methodDonated);
		bal.save();
	}

	// Return 20000 is the target amount of donations needed to achieve 100% 
	private static long getDonationTarget() 
	{
		return 20000;
	}

	//List donations.
	//Progress bar (total * 100 / target)
	//Return progress
	public static String getPercentTargetAchieved() 
	{
		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation donation : allDonations) 
		{
			total += donation.received;
		}
		long target = getDonationTarget();
		long percentachieved = (total * 100 / target);
		String progress = String.valueOf(percentachieved);
		Logger.logInfo("Percent of target achieved (string) " + progress + "percentachieved (long)= " + percentachieved);
		return progress;
	}

	//If user is null then go to accounts.login page
	//else list candidates. list donations. Render donations, user, candidates.
	public static void renderReport() 
	{
		User user = Accounts.getCurrentUser();
		if (user == null) 
        {
		    Accounts.login();
		} 
		else 
		{
		    List<Candidate> candidates = Candidate.findAll();
		    List donations = (List) Donation.findAll();
		    render(donations, user, candidates);
		}
	}
	
	//List candidate. Get user. List donation.
	//Find Candidate.
	//renderTemplate makes a copy of the render report page
	public static void candidatesFilter(String cand) 
	{
		{
			List<Candidate> candidates = Candidate.findAll();
			User user = Accounts.getCurrentUser();
			List<Donation> donations = new ArrayList();
			List<Donation> allDon = Donation.findAll();
			Candidate selCand = Candidate.findById(Long.parseLong(cand));
			for (Donation donation : allDon) 
			{
		        if (donation.from.candidate == selCand) 
				{
				    donations.add(donation);
				}
			}
			renderTemplate("DonationController/renderReport.html", user, donations, selCand, candidates);
		}
	}
	
}