package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class EditProfile extends Controller 
{
    //Get current user
	//render user
	public static void index()
	{
		User user = Accounts.getCurrentUser();
		Logger.info("Landed in EditProfile class");
		render(user);
	}
	
	//If userId is null then go to Accounts.index
	//List of inputs. User is then saved and then it goes back to the EditProfile page
	public static void edit
	(String firstName, String lastName, int age, String addressLine1, String addressLine2, String city, String state, String zipCode)
    {
	    String userId = session.get("logged_in_userid");
		if (userId == null)
		{
		    Accounts.index();
		}
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		user.lastName = lastName;
		user.age = age;
		user.addressLine1 = addressLine1;
		user.addressLine2 = addressLine2;
		user.city = city;
		user.state = state;
		user.zipCode = zipCode;
		 
		Logger.info
		(firstName + " " + lastName + " " + age + " " + addressLine1 + " " + addressLine2 + " " + city + " " + state + " " + zipCode + " ");
		user.save();
		EditProfile.index();
	}
}