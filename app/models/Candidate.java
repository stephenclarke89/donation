package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import play.db.jpa.Model;
import java.util.List;
import java.util.ArrayList;

@Entity
public class Candidate extends Model
{
	//Candidate info
	//One to Many Relationship
	//List user
	public String firstName;
	public String lastName;
	public String email;
	
	@OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
    List<User> users = new ArrayList<User>();
	
	public Candidate( String firstName, String lastName, String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
}