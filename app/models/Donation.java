package models;  

import play.db.jpa.Blob;  

import javax.persistence.Entity;   

import javax.persistence.Table;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import play.db.jpa.*;
import play.db.jpa.Model;
import java.util.List;
import java.util.ArrayList;

@Entity
public class Donation extends Model
{
	//Donation info
	//Many to One Relationship
	public long received;
	public String methodDonated;
	@ManyToOne
	public User from;
	
	public Donation(User from, long received, String methodDonated)
	{
		this.received = received;
		this.methodDonated = methodDonated;
		this.from = from;
	}
}