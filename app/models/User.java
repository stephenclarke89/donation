package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
public class User extends Model 
{
	public boolean usaCitizen;
	public String firstName;
	public String lastName;
	public String state;
	public int age;
	@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	List<Donation> donations = new ArrayList<Donation>();
	
	@ManyToOne
	public Candidate candidate;
	public String email;
	public String password;
	public String addressLine1;
	public String addressLine2;
	public String city;
	public String zipCode;

	public User
	(boolean usaCitizen, String firstName, String lastName, String state, int age,
	String email, String password, String addressLine1, String addressLine2, String city, String zipCode) 
	{
		this.usaCitizen = usaCitizen;
		this.firstName = firstName;
		this.lastName = lastName;
		this.state = state;
		this.age = age;
		this.email = email;
		this.password = password;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.zipCode = zipCode;
	}

	public static User findByEmail(String email) 
	{
		return find("email", email).first();
	}

	public boolean checkPassword(String password) 
	{
		return this.password.equals(password);
	}
}